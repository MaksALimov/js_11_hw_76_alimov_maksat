import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from "react-redux";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import sendMessageReducer from "./store/reducers/sendMessageReducer";
import messagesReducer from "./store/reducers/messagesReducer";
import {CssBaseline} from "@material-ui/core";

const rootReducer = combineReducers({
    sendMessage: sendMessageReducer,
    getMessages: messagesReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

const app = (
    <Provider store={store}>
        <CssBaseline/>
        <App/>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
