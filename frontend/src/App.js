import SendMessage from "./components/SendMessage/SendMessage";
import Messages from "./components/Messages/Messages";
import Layout from "./components/UI/Layout/Layout";
import {Container} from "@material-ui/core";

const App = () => (
    <Layout>
        <Container>
            <SendMessage/>
            <Messages/>
        </Container>
    </Layout>
);

export default App;
