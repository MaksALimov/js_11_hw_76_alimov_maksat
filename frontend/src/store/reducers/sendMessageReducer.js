import {
    POST_MESSAGE_FAILURE,
    POST_MESSAGE_REQUEST,
    POST_MESSAGE_SUCCESS
} from "../actions/sendMessageActions";

const initialState = {
    error: null,
    loading: false,
};

const sendMessageReducer = (state = initialState, action) => {
    switch (action.type) {
        case POST_MESSAGE_REQUEST: {
            return {...state, error: false, loading: true};
        }

        case POST_MESSAGE_SUCCESS: {
            return {...state, error: false, loading: false};
        }

        case POST_MESSAGE_FAILURE: {
            return {...state, error: action.payload, loading: false};
        }

        default:
            return state;
    }
};

export default sendMessageReducer;
