import {GET_ALL_MESSAGES_FAILURE, GET_ALL_MESSAGES_REQUEST, GET_ALL_MESSAGES_SUCCESS} from "../actions/MessagesActions";

const initialState = {
    error: null,
    messages: [],
    lastDate: null,
};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_MESSAGES_REQUEST: {
            return {...state, error: false};
        }

        case GET_ALL_MESSAGES_SUCCESS: {
            return {
                ...state, error: false,
                messages: [...state.messages.slice(-30), ...action.payload],
                lastDate: action.payload.length > 0 ? action.payload[action.payload.length - 1].datetime : state.lastDate,
            };
        }

        case GET_ALL_MESSAGES_FAILURE: {
            return {...state, error: action.payload};
        }

        default:
            return state;
    }
};

export default messagesReducer;