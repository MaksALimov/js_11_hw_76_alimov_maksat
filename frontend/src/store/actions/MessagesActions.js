import axiosApi from "../../axiosApi";

export const GET_ALL_MESSAGES_REQUEST = 'GET_ALL_MESSAGES_REQUEST';
export const GET_ALL_MESSAGES_SUCCESS = 'GET_ALL_MESSAGES_SUCCESS';
export const GET_ALL_MESSAGES_FAILURE = 'GET_ALL_MESSAGES_FAILURE';

export const getMessagesRequest = () => ({type: GET_ALL_MESSAGES_REQUEST});
export const getMessagesSuccess = messages => ({type: GET_ALL_MESSAGES_SUCCESS, payload: messages});
export const getMessagesFailure = error => ({type: GET_ALL_MESSAGES_FAILURE, payload: error});

export const getAllMessages = () => {
    return async (dispatch, getState) => {
        const {lastDate} = getState().getMessages;

        let url = 'http://127.0.0.1:8088/messages';

        if (lastDate) {
            url += '?datetime=' + lastDate;
        }
        try {
            dispatch(getMessagesRequest());

            const response = await axiosApi.get(url);
            dispatch(getMessagesSuccess(response.data));
        } catch (error) {
            dispatch(getMessagesFailure(error));
        }
    };
};