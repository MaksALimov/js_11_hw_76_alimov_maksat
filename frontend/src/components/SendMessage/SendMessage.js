import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {sendMessage} from "../../store/actions/sendMessageActions";
import SendIcon from '@material-ui/icons/Send';
import {Button, CircularProgress, Grid, makeStyles, TextField} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    formSpacing: {
        marginTop: theme.spacing(7),
    },
    spinner: {
        margin: '30px auto',
    }
}));

const SendMessage = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [message, setMessage] = useState({
        author: '',
        message: '',
    });

    const loading = useSelector(state => state.sendMessage.loading);
    let error = useSelector(state => state.sendMessage.error);

    const onInputChange = e => {
        const {name, value} = e.target;
        setMessage(prevState => ({...prevState, [name]: value}));
    };

    const onSubmit = e => {
        e.preventDefault();
        dispatch(sendMessage(message));
        setMessage(prevState => ({...prevState, author: '', message: ''}));
    };

    return (
        <form onSubmit={onSubmit} className={classes.formSpacing}>
            <Grid item container direction="column">
                <Grid item xs container justifyContent="center" alignItems="center" spacing={10}>
                    <Grid item>
                        <TextField
                            error={!!error}
                            variant="outlined"
                            name="author"
                            value={message.author}
                            label="Author"
                            onChange={onInputChange}
                            helperText={error ? "Заполните пожалуйста поле" : null}
                        />
                    </Grid>
                    <Grid item>
                        <TextField
                            error={!!error}
                            variant="outlined"
                            name="message"
                            label="Message"
                            value={message.message}
                            onChange={onInputChange}
                            multiline
                            helperText={error ? "Заполните пожалуйста поле" : null}
                        />
                    </Grid>
                    <Grid item>
                        <Button
                            variant="contained"
                            type="submit"
                            color="primary"
                            endIcon={<SendIcon/>}
                        >
                            Send
                        </Button>
                    </Grid>
                </Grid>
                {loading ? <Grid item className={classes.spinner}><CircularProgress/></Grid> : null}
            </Grid>
        </form>
    );
};

export default SendMessage;