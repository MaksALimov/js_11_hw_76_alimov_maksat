import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getAllMessages} from "../../store/actions/MessagesActions";
import dayjs from "dayjs";
import {Grid, makeStyles, Paper, Typography} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    paper: {
        padding: '20px',
        margin: '10px',
        borderRadius: '15px 30px 45px',
        boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px',
    },
    user: {
        margin: '10px 0',
    },
    container: {
        marginTop: '50px',
    },
}))

const Messages = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const allMessages = useSelector(state => state.getMessages.messages);

    useEffect(() => {

        dispatch(getAllMessages());

        const interval = setInterval(() => {
            dispatch(getAllMessages());
        }, 2000);

        return () => {
            clearInterval(interval);
        }
    }, [dispatch]);


    return (
        <Grid item container direction="column" className={classes.container}>
            {allMessages.map(message => (
                <Grid item key={message.id}>
                    <Paper className={classes.paper}>
                        <Typography
                            className={classes.user}
                            variant="h5">
                            Author: {message.author}
                        </Typography>
                        <Typography
                            className={classes.user}
                            variant="body1">
                            Message: {message.message}
                        </Typography>
                        <Typography
                            className={classes.user}
                            variant="body1">
                            Datetime: {dayjs(new Date(message.datetime)).format('YYYY-MM-DD HH:mm:ss')}
                        </Typography>
                    </Paper>
                </Grid>
            ))}
        </Grid>
    );
};

export default Messages;