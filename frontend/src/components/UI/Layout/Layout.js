import React from 'react';
import {AppBar, Toolbar, Typography} from "@material-ui/core";

const Layout = ({children}) => {
    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                    <Typography variant="h6">
                        Chat
                    </Typography>
                </Toolbar>
            </AppBar>
            <Toolbar/>
            {children}
        </>
    );
};

export default Layout;