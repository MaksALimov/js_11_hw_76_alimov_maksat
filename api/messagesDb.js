const {nanoid} = require('nanoid');
const fs = require('fs');
const filename = './db.json';

let messagesData = [];

module.exports = {
    init() {
        try {
            const fileContents = fs.readFileSync(filename);
            messagesData = JSON.parse(fileContents);
        } catch (e) {
            messagesData = [];
        }
    },

    addMessage(message) {
        message.id = nanoid();
        message.datetime = new Date().toISOString();
        messagesData.push(message);
        this.saveMessages();
        return message;
    },

    getAllMessages() {
        return messagesData.slice(-30);
    },

    saveMessages() {
        fs.writeFileSync(filename, JSON.stringify(messagesData));
    }
}