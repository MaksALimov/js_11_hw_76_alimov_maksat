const express = require('express');
const messagesDb = require('../messagesDb');
const router = express.Router();

router.get('/', (req, res) => {
    const allMessages = messagesDb.getAllMessages();
    const datetime = req.query.datetime;
    const date = new Date(datetime);
    const index = allMessages.findIndex(message => message.datetime === datetime);

    const queryDatetime = 'datetime';

    const possibleCombinations = (str) => {
        let combinations = [];
        for (let i = 0; i < str.length; i++) {
            for (let j = i + 1; j < str.length + 1; j++) {
                combinations.push(str.slice(i, j));
            }
        }
        return combinations;
    }

    const allVariations = possibleCombinations(queryDatetime);
    const concurrence = allVariations.findIndex(word => word === Object.keys(req.query).join(''));

    if (Object.keys(req.query).join('') !== allVariations[concurrence] && index === -1) {
        return res.send(allMessages);
    }

    if (!isNaN(date.getDate()) && allMessages[index] !== undefined) {
        const messages = [];

        for (let i = index + 1; i < allMessages.length; i++) {
            messages.push(allMessages[i]);
        }
        return res.send(messages);
    }


    if (isNaN(date.getDate())) {
        return res.status(400).send({error: "Data is not valid"});
    }

    res.status(400).send({error: "Data is not valid"});
});

router.post('/', (req, res) => {
    if (!req.body.message || !req.body.author) {
        return res.status(400).send({error: "Author and message must be present in the request"});
    }

    const newMessage = messagesDb.addMessage({
        message: req.body.message,
        author: req.body.author,
    });

    res.send(newMessage);
});

module.exports = router;