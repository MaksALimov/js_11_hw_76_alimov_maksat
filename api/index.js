const express = require('express');
const app = express();
const cors = require('cors');
const messagesDb = require('./messagesDb');
const messages = require('./app/messages');

const port = 8088;

app.use(cors());
app.use(express.json());
app.use('/messages', messages);

messagesDb.init();
app.listen(port, () => {
   console.log(`Server created on ${port} port!`);
});